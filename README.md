# Coworking_Bars #

## Elastic Search 7.2.1 ##

Modify configuration file elasticsearch.yml:\
&nbsp;&nbsp;cluster.name: coworking-cluster\
&nbsp;&nbsp;node.name: coworking-node\
&nbsp;&nbsp;path.data: [path where to put data]\
&nbsp;&nbsp;path.logs: [path where to put log files]\
&nbsp;&nbsp;path.repo: [path where to put saves]

Restart Elastic Search to take into account changes.

Check connexion:\
`curl -X GET 'localhost:9200'`

Create the "bars" index:\
`curl -X PUT "localhost:9200/bars"`

Create mapping for "bars" index:\
`curl -X PUT "localhost:9200/bars/_mapping" -H 'Content-Type: application/json' -d '
{
    "properties": {
        "name": {
          "type": "text"
        },
        "address": {
          "type": "text"
        },
        "position": {
          "type": "geo_point"
        },
        "schedules": {
          "type": "nested",
          "properties": {
            "opens_at": {
              "type": "date",
              "format": "HH:mm:ss"
            },
            "closes_at": {
              "type": "date",
              "format": "HH:mm:ss"
            }
          }
        },
        "price": {
          "type": "float"
        }
    }
}'`

Insert a bar:\
`curl -X POST 'localhost:9200/bars/_doc/[id]' -H 'Content-Type: application/json' -d
'{
    "name" : "",
    "adress" : "",
    "position" : "[latitude],[longitude]",
    "schedules" : { "opens_at" : "[hours:minutes:seconds]", "closes_at" : "[hours:minutes:seconds]" },
    "price" : ""
}'`

Create the "bookings" index:\
`curl -X PUT "localhost:9200/bookings"`

Create mapping for "bookings" index:\
`curl -X PUT "localhost:9200/bookings/_mapping" -H 'Content-Type: application/json' -d '
{
    "properties": {
        "place_name": {
          "type": "text"
        },
        "user_name": {
          "type": "text"
        },
        "user_enterprise": {
          "type": "text"
        },
        "place_price": {
          "type": "float"
        }
    }
}'`

Insert a booking:\
`curl -X POST 'localhost:9200/bookings/_doc/[id]' -H 'Content-Type: application/json' -d
'{
    "place_name" : "",
    "user_name" : "",
    "user_enterprise" : "",
    "place_price" : ""
}'`

Create the "users" index:\
`curl -X PUT "localhost:9200/users"`

Create mapping for "users" index:\
`curl -X PUT "localhost:9200/users/_mapping" -H 'Content-Type: application/json' -d '
{
    "properties": {
        "position": {
          "type": "geo_point"
        },
        "name": {
          "type": "text"
        },
        "enterprise": {
          "type": "text"
        }
    }
}'`

Insert a user:\
`curl -X POST 'localhost:9200/users/_doc/[id]' -H 'Content-Type: application/json' -d
'{
    "position" : "[latitude],[longitude]",
    "name" : "",
    "enterprise" : ""
}'`

For deleting a document:\
`curl -X DELETE "localhost:9200/[index]/_doc/[id]"`

For deleting an index:\
`curl -X DELETE "localhost:9200/[index]"`

For searching with parameters:\
`curl -X GET "localhost:9200/[index]/_search?q=[key]:[value]"`

## API Spring ##

To access API documentation:\
http://localhost:8080/swagger-ui.html \
JSON version:\
http://localhost:8080/v2/api-docs


