package com.coworking.coworking.booking;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookingControllerTests
{
    @LocalServerPort
    int randomServerPort;

    @Test
    public void findAllBookingsSuccess() throws URISyntaxException
    {
        // Arrange
        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:" + randomServerPort + "/bookings?limit=5";
        URI uri = new URI(baseUrl);

        // Act
        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

        // Assert
        Assert.assertEquals(200, result.getStatusCodeValue());
    }
}
