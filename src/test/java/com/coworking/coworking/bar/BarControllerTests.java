package com.coworking.coworking.bar;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import java.net.URI;
import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BarControllerTests
{
    @LocalServerPort
    int randomServerPort;

    @Test
    public void findBarsByLocationSuccess() throws URISyntaxException
    {
        // Arrange
        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:" + randomServerPort + "/bars";
        URI uri = new URI(baseUrl);

        // Act
        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

        // Assert
        Assert.assertEquals(200, result.getStatusCodeValue());
    }
}
