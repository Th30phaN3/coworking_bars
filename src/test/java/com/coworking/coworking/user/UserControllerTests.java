package com.coworking.coworking.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTests
{
    @LocalServerPort
    int randomServerPort;

    @Test
    public void findUserByIdError()
    {
        // Arrange

        // Act

        // Assert
    }
}
