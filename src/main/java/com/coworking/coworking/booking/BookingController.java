package com.coworking.coworking.booking;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.List;

@Api(description = "CRUD operations on bookings", consumes = "application/json", produces = "application/json")
@RestController
@RequestMapping("/bookings")
public class BookingController {

    @Autowired BookingService bookingService;

    @ApiOperation(value = "Insert a new booking")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Resource created"),
        @ApiResponse(code = 500, message = "Cannot create the resource") })
    @PostMapping
    public ResponseEntity<?> create(@RequestBody Booking booking) {
        Booking result = bookingService.create(booking);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(result.getId())
            .toUri();

        // Send location in response
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Delete a booking")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource deleted"),
        @ApiResponse(code = 404, message = "Cannot find resource"),
        @ApiResponse(code = 500, message = "Cannot delete the resource") })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id) {
        Integer result = bookingService.delete(id);

        if (result == 1)
            return ResponseEntity.notFound().build();

        if (result == 2)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Modify a booking")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Resource modified"),
        @ApiResponse(code = 500, message = "Cannot modify the resource") })
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody Booking booking) {
        Booking result = bookingService.update(id, booking);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Find a booking with its identifier")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource found"),
        @ApiResponse(code = 404, message = "Cannot find resource") })
    @GetMapping("/{id}")
    public ResponseEntity<?> findBookingById(@PathVariable("id") String id) {
        Booking result = bookingService.findById(id);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Find all bookings")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resources found"),
        @ApiResponse(code = 500, message = "Cannot find the resources") })
    @GetMapping
    public ResponseEntity<?> findAllBookings(@RequestParam(name = "limit", required = false) Integer limit)
    {
        List<Booking> results = bookingService.findAll(limit);

        if (results == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity<>(results, HttpStatus.OK);
    }
}
