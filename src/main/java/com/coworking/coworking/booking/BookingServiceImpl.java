package com.coworking.coworking.booking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    public Booking create(Booking booking) {
        return bookingRepository.create(booking);
    }

    public Integer delete(String id) {
        return bookingRepository.delete(id);
    }

    public Booking update(String id, Booking booking) {
        return bookingRepository.update(id, booking);
    }

    public Booking findById(String id) {
        return bookingRepository.findById(id);
    }

    public List<Booking> findAll(Integer limit) {
        // Limit results to 30 by default
        if (limit == null || limit < 0)
            limit = 30;
        return bookingRepository.findAll(limit);
    }
}
