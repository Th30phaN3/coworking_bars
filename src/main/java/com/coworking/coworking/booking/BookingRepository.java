package com.coworking.coworking.booking;

import java.util.List;

public interface BookingRepository {

    Booking create(Booking booking);

    Integer delete(String id);

    Booking update(String id, Booking booking);

    Booking findById(String id);

    List<Booking> findAll(Integer limit);
}
