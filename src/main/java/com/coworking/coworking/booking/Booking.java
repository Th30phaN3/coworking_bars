package com.coworking.coworking.booking;

import io.swagger.annotations.ApiModelProperty;

public class Booking {

    @ApiModelProperty(notes = "Identifier", allowableValues = "range[0,infinity]", example = "3")
    private long id;

    @ApiModelProperty(notes = "Bar name", required = true, example = "Awesome Bar")
    private String placeName;

    @ApiModelProperty(notes = "User enterprise", required = true, example = "ThisIsATest")
    private String userEnterprise;

    @ApiModelProperty(notes = "User name", required = true, example = "John Smith")
    private String userName;

    @ApiModelProperty(notes = "Bar price per hour", allowableValues = "range[0,infinity]", required = true, example = "5")
    private float placePrice;

    public Booking()
    {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getUserEnterprise() {
        return userEnterprise;
    }

    public void setUserEnterprise(String userEnterprise) {
        this.userEnterprise = userEnterprise;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Float getPlacePrice() {
        return placePrice;
    }

    public void setPlacePrice(float placePrice) {
        this.placePrice = placePrice;
    }
}
