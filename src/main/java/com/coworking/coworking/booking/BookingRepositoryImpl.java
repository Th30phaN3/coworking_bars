package com.coworking.coworking.booking;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Repository
public class BookingRepositoryImpl implements BookingRepository {

    Logger logger = LoggerFactory.getLogger(BookingRepositoryImpl.class);

    @Autowired
    private RestHighLevelClient ESClient;

    public Booking create(Booking booking) {
        Map<String, Object> bookingMap = getBookingAsJson(booking);

        IndexRequest indexRequest = new IndexRequest("bookings").id(booking.getId().toString()).source(bookingMap);
        indexRequest.timeout("10s");
        indexRequest.opType(DocWriteRequest.OpType.CREATE);

        IndexResponse indexResponse = null;
        try
        {
            indexResponse = ESClient.index(indexRequest, RequestOptions.DEFAULT);

        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.CONFLICT)
                logger.error("Modifying resource is not allowed.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot create resource: " + ex);
            return null;
        }

        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return null;
        }

        if (indexResponse.getResult() != DocWriteResponse.Result.CREATED)
        {
            logger.error("Failed to create resource: " + indexResponse.getResult());
            return null;
        }

        String createdId = indexResponse.getId();
        Booking createdResource = findById(createdId);

        return createdResource;
    }

    public Integer delete(String id) {
        DeleteRequest deleteRequest = new DeleteRequest("bookings", id);
        deleteRequest.timeout("10s");

        DeleteResponse deleteResponse = null;
        try
        {
            deleteResponse = ESClient.delete(deleteRequest, RequestOptions.DEFAULT);
        }
        catch (IOException ex)
        {
            logger.error("Cannot delete resource: " + ex);
            return 2;
        }

        ReplicationResponse.ShardInfo shardInfo = deleteResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return 1;
        }

        if (deleteResponse.getResult() != DocWriteResponse.Result.DELETED) {
            logger.error("Failed to delete resource: " + deleteResponse.getResult());
            return 1;
        }

        return 0;
    }

    public Booking update(String id, Booking booking) {
        Map<String, Object> bookingMap = getBookingAsJson(booking);

        IndexRequest indexRequest = new IndexRequest("bookings").id(id).source(bookingMap);
        indexRequest.timeout("10s");
        indexRequest.opType(DocWriteRequest.OpType.INDEX);

        IndexResponse indexResponse = null;
        try
        {
            indexResponse = ESClient.index(indexRequest, RequestOptions.DEFAULT);
        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.CONFLICT)
                logger.error("Creating resource is not allowed.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot modify resource: " + ex);
            return null;
        }

        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return null;
        }

        if (indexResponse.getResult() != DocWriteResponse.Result.UPDATED)
        {
            logger.error("Failed to modify resource: " + indexResponse.getResult());
            return null;
        }

        String modifiedId = indexResponse.getId();
        Booking modifiedResource = findById(modifiedId);

        return modifiedResource;
    }

    public Booking findById(String id) {
        GetRequest getRequest = new GetRequest("bookings", id);
        GetResponse getResponse = null;

        try
        {
            getResponse = ESClient.get(getRequest, RequestOptions.DEFAULT);
        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.NOT_FOUND)
                logger.error("Cannot find index.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot get resource: " + ex);
            return null;
        }

        if (!getResponse.isExists()) {
            logger.error("The specified resource does not exists.");
            return null;
        }

        String resourceId = getResponse.getId();
        String sourceAsString = getResponse.getSourceAsString();
        Booking booking = getJsonAsBooking(sourceAsString, resourceId);

        return booking;
    }

    public List<Booking> findAll(Integer limit) {
        SearchRequest searchRequest = new SearchRequest("bookings");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(limit);
        searchSourceBuilder.timeout(new TimeValue(10, TimeUnit.SECONDS));
        searchSourceBuilder.query(QueryBuilders.matchAllQuery()); // Return all documents
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = null;
        try
        {
            searchResponse = ESClient.search(searchRequest, RequestOptions.DEFAULT);
        }
        catch (IOException ex)
        {
            logger.error("Cannot get resources: " + ex);
            return null;
        }

        if (searchResponse.isTimedOut())
        {
            logger.error("The search has timed out.");
            return null;
        }

        List<Booking> allBookings = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit hit : searchHits) {
            String sourceAsString = hit.getSourceAsString();
            Booking booking = getJsonAsBooking(sourceAsString, hit.getId());
            allBookings.add(booking);
        }

        return allBookings;
    }

    private Map<String, Object> getBookingAsJson(Booking booking) {
        Map<String, Object> bookingMap = new HashMap<>();
        bookingMap.put("place_name", booking.getPlaceName());
        bookingMap.put("place_price", booking.getPlacePrice());
        bookingMap.put("user_name", booking.getUserName());
        bookingMap.put("user_enterprise", booking.getUserEnterprise());
        return bookingMap;
    }

    private Booking getJsonAsBooking(String bookingAsString, String id) {
        JSONObject bookingAsJson = new JSONObject(bookingAsString);
        Booking booking = new Booking();

        booking.setId(Long.parseLong(id));
        booking.setPlaceName(bookingAsJson.getString("place_name"));
        booking.setPlacePrice(bookingAsJson.getFloat("place_price"));
        booking.setUserName(bookingAsJson.getString("user_name"));
        booking.setUserEnterprise(bookingAsJson.getString("user_enterprise"));

        return booking;
    }
}
