package com.coworking.coworking.bar;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.GeoDistanceSortBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Repository
public class BarRepositoryImpl implements BarRepository {

    Logger logger = LoggerFactory.getLogger(BarRepositoryImpl.class);

    @Autowired
    private RestHighLevelClient ESClient;

    public Bar create(Bar bar) {
        Map<String, Object> barMap = getBarAsJson(bar);

        IndexRequest indexRequest = new IndexRequest("bars").id(bar.getId().toString()).source(barMap);
        indexRequest.timeout("10s");
        indexRequest.opType(DocWriteRequest.OpType.CREATE);

        IndexResponse indexResponse = null;
        try
        {
            indexResponse = ESClient.index(indexRequest, RequestOptions.DEFAULT);

        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.CONFLICT)
                logger.error("Modifying resource is not allowed.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot create resource: " + ex);
            return null;
        }

        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return null;
        }

        if (indexResponse.getResult() != DocWriteResponse.Result.CREATED)
        {
            logger.error("Failed to create resource: " + indexResponse.getResult());
            return null;
        }

        String createdId = indexResponse.getId();
        Bar createdResource = findById(createdId);

        return createdResource;
    }

    public Integer delete(String id) {

        DeleteRequest deleteRequest = new DeleteRequest("bars", id);
        deleteRequest.timeout("10s");

        DeleteResponse deleteResponse = null;
        try
        {
            deleteResponse = ESClient.delete(deleteRequest, RequestOptions.DEFAULT);
        }
        catch (IOException ex)
        {
            logger.error("Cannot delete resource: " + ex);
            return 2;
        }

        ReplicationResponse.ShardInfo shardInfo = deleteResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return 1;
        }

        if (deleteResponse.getResult() != DocWriteResponse.Result.DELETED) {
            logger.error("Failed to delete resource: " + deleteResponse.getResult());
            return 1;
        }

        return 0;
    }

    public Bar update(String id, Bar bar) {
        Map<String, Object> barMap = getBarAsJson(bar);

        IndexRequest indexRequest = new IndexRequest("bars").id(id).source(barMap);
        indexRequest.timeout("10s");
        indexRequest.opType(DocWriteRequest.OpType.INDEX);

        IndexResponse indexResponse = null;
        try
        {
            indexResponse = ESClient.index(indexRequest, RequestOptions.DEFAULT);
        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.CONFLICT)
                logger.error("Creating resource is not allowed.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot modify resource: " + ex);
            return null;
        }

        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return null;
        }

        if (indexResponse.getResult() != DocWriteResponse.Result.UPDATED)
        {
            logger.error("Failed to modify resource: " + indexResponse.getResult());
            return null;
        }

        String modifiedId = indexResponse.getId();
        Bar modifiedResource = findById(modifiedId);

        return modifiedResource;
    }

    public Bar findById(String id) {
        GetRequest getRequest = new GetRequest("bars", id);
        GetResponse getResponse = null;

        try
        {
            getResponse = ESClient.get(getRequest, RequestOptions.DEFAULT);
        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.NOT_FOUND)
                logger.error("Cannot find index.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot get resource: " + ex);
            return null;
        }

        if (!getResponse.isExists()) {
            logger.error("The specified resource does not exists.");
            return null;
        }

        String resourceId = getResponse.getId();
        String sourceAsString = getResponse.getSourceAsString();
        Bar bar = getJsonAsBar(sourceAsString, resourceId);

        return bar;
    }

    public List<Bar> findAll(Integer limit) {
        return find(null, null, limit);
    }

    public List<Bar> findByPosition(Double latitude, Double longitude, Integer limit) {
        return find(latitude, longitude, limit);
    }

    private List<Bar> find(Double latitude, Double longitude, Integer limit)
    {
        SearchRequest searchRequest = new SearchRequest("bars");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(limit);
        searchSourceBuilder.timeout(new TimeValue(10, TimeUnit.SECONDS));
        if (latitude != null && longitude != null)
        {
            searchSourceBuilder.query(QueryBuilders.geoDistanceQuery("position").point(latitude,
                longitude).distance(10, DistanceUnit.KILOMETERS)); // Search within a 10-kilometre radius
            searchSourceBuilder.sort(new GeoDistanceSortBuilder("position", latitude, longitude));
        }
        else
        {
            searchSourceBuilder.query(QueryBuilders.matchAllQuery()); // Return all documents
        }
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = null;
        try
        {
            searchResponse = ESClient.search(searchRequest, RequestOptions.DEFAULT);
        }
        catch (IOException ex)
        {
            logger.error("Cannot get resources: " + ex);
            return null;
        }

        if (searchResponse.isTimedOut())
        {
            logger.error("The search has timed out.");
            return null;
        }

        List<Bar> allBars = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit hit : searchHits) {
            String sourceAsString = hit.getSourceAsString();
            Bar bar = getJsonAsBar(sourceAsString, hit.getId());
            allBars.add(bar);
        }

        return allBars;
    }

    private Map<String, Object> getBarAsJson(Bar bar) {
        Map<String, Object> scheduleMap = new HashMap<>();
        scheduleMap.put("opens_at", bar.getOpeningHour());
        scheduleMap.put("closes_at", bar.getClosingHour());

        Map<String, Object> barMap = new HashMap<>();
        barMap.put("name", bar.getName());
        barMap.put("address", bar.getAddress());
        barMap.put("position", bar.getLatitude() + "," + bar.getLongitude());
        barMap.put("schedules", scheduleMap);
        barMap.put("price", bar.getPrice());

        return barMap;
    }

    private Bar getJsonAsBar(String barAsString, String id) {

        JSONObject barAsJson = new JSONObject(barAsString);
        JSONObject schedulesAsJSON = (JSONObject)barAsJson.get("schedules");
        Bar bar = new Bar();

        bar.setId(Long.parseLong(id));
        bar.setName(barAsJson.getString("name"));
        bar.setAddress(barAsJson.getString("address"));
        bar.setPrice(barAsJson.getFloat("price"));

        String position = barAsJson.getString("position");
        String allPositions[] = position.split(",");
        Double latitude = Double.parseDouble(allPositions[0]);
        Double longitude = Double.parseDouble(allPositions[1]);
        bar.setLatitude(latitude);
        bar.setLongitude(longitude);

        bar.setOpeningHour(schedulesAsJSON.getString("opens_at"));
        bar.setClosingHour(schedulesAsJSON.getString("closes_at"));

        return bar;
    }
}
