package com.coworking.coworking.bar;

import io.swagger.annotations.ApiModelProperty;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Bar {

    @ApiModelProperty(notes = "Identifier", allowableValues = "range[0,infinity]", example = "9")
    private long id;

    @ApiModelProperty(notes = "Bar name", required = true, example = "Awesome Bar")
    private String name;

    @ApiModelProperty(notes = "Bar complete address", required = true, example = "3 bis Great Street")
    private String address;

    @ApiModelProperty(notes = "Latitude (WGS84)", required = true, example = "47.216")
    private double latitude;

    @ApiModelProperty(notes = "Longitude (WGS84)", required = true, example = "-1.549")
    private double longitude;

    @ApiModelProperty(notes = "Price per hour", allowableValues = "range[0,infinity]", required = true, example = "7")
    private float price;

    @ApiModelProperty(notes = "Opening hour", required = true, example = "08:00:00")
    private Date openingHour;

    @ApiModelProperty(notes = "Closing hour", required = true, example = "19:00:00")
    private Date closingHour;

    public Bar()
    {
        super();
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getOpeningHour() {
        SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
        return ft.format(openingHour);
    }

    public void setOpeningHour(String openingHour) {
        SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
        try {
            this.openingHour = ft.parse(openingHour);
        }
        catch (ParseException e)
        {
            this.openingHour = new Date();
        }
    }

    public String getClosingHour() {
        SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
        return ft.format(closingHour);
    }

    public void setClosingHour(String closingHour) {
        SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
        try {
            this.closingHour = ft.parse(closingHour);
        }
        catch (ParseException e)
        {
            this.closingHour = new Date();
        }
    }
}
