package com.coworking.coworking.bar;

import java.net.URI;
import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Api(description = "CRUD operations on bars", consumes = "application/json", produces = "application/json")
@RestController
@RequestMapping("/bars")
public class BarController {

    @Autowired
    BarService barService;

    @ApiOperation(value = "Insert a new bar")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Resource created"),
        @ApiResponse(code = 500, message = "Cannot create the resource") })
    @PostMapping
    public ResponseEntity<?> create(@RequestBody Bar bar) {
        Bar result = barService.create(bar);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(result.getId())
            .toUri();

        // Send location in response
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Delete a bar")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource deleted"),
        @ApiResponse(code = 404, message = "Cannot find resource"),
        @ApiResponse(code = 500, message = "Cannot delete the resource") })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id) {
        Integer result = barService.delete(id);

        if (result == 1)
            return ResponseEntity.notFound().build();

        if (result == 2)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Modify a bar")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Resource modified"),
        @ApiResponse(code = 500, message = "Cannot modify the resource") })
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody Bar bar) {
        Bar result = barService.update(id, bar);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Find a bar with its identifier")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource found"),
        @ApiResponse(code = 404, message = "Cannot find resource") })
    @GetMapping("/{id}")
    public ResponseEntity<?> findBarById(@PathVariable("id") String id) {
        Bar result = barService.findById(id);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Find bars according to location (or all if not provided)")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resources found"),
        @ApiResponse(code = 400, message = "Bad request"),
        @ApiResponse(code = 500, message = "Cannot find the resources") })
    @GetMapping
    public ResponseEntity<?> findBarsByLocation(@RequestParam(name = "latitude", required = false) Double latitude,
                                                @RequestParam(name = "longitude", required = false) Double longitude,
                                                @RequestParam(name = "limit", required = false) Integer limit)
    {
        if (positionNotValid(latitude, longitude))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        if (latitude == null && longitude == null) {
            List<Bar> results = barService.findAll(limit);

            if (results == null)
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

            return new ResponseEntity<>(results, HttpStatus.OK);
        }

        List<Bar> results = barService.findByPosition(latitude, longitude, limit);

        if (results == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    private boolean positionNotValid(Double latitude, Double longitude) {
        return (latitude == null && longitude != null) ||
               (longitude == null  && latitude != null);
    }
}
