package com.coworking.coworking.bar;

import java.util.List;

public interface BarService {

    Bar create(Bar bar);

    Integer delete(String id);

    Bar update(String id, Bar bar);

    Bar findById(String id);

    List<Bar> findAll(Integer limit);

    List<Bar> findByPosition(Double latitude, Double longitude, Integer limit);
}
