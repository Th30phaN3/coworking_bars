package com.coworking.coworking.bar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BarServiceImpl implements BarService {

    @Autowired
    private BarRepository barRepository;

    public Bar create(Bar bar) {
        return barRepository.create(bar);
    }

    public Integer delete(String id) {
        return barRepository.delete(id);
    }

    public Bar update(String id, Bar bar) {
        return barRepository.update(id, bar);
    }

    public Bar findById(String id) {
        return barRepository.findById(id);
    }

    public List<Bar> findAll(Integer limit) {
        // Limit results to 30 by default
        if (limit == null || limit < 0)
            limit = 30;
        return barRepository.findAll(limit);
    }

    public List<Bar> findByPosition(Double latitude, Double longitude, Integer limit) {
        // Limit results to 10 by default
        if (limit == null || limit < 0)
            limit = 10;
        return barRepository.findByPosition(latitude, longitude, limit);
    }
}
