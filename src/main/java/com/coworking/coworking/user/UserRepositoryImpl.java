package com.coworking.coworking.user;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class UserRepositoryImpl implements UserRepository {

    Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);

    @Autowired
    private RestHighLevelClient ESClient;

    public User create(User user) {
        Map<String, Object> userMap = getUserAsJson(user);

        IndexRequest indexRequest = new IndexRequest("users").id(user.getId().toString()).source(userMap);
        indexRequest.timeout("10s");
        indexRequest.opType(DocWriteRequest.OpType.CREATE);

        IndexResponse indexResponse = null;
        try
        {
            indexResponse = ESClient.index(indexRequest, RequestOptions.DEFAULT);

        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.CONFLICT)
                logger.error("Modifying resource is not allowed.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot create resource: " + ex);
            return null;
        }

        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return null;
        }

        if (indexResponse.getResult() != DocWriteResponse.Result.CREATED)
        {
            logger.error("Failed to create resource: " + indexResponse.getResult());
            return null;
        }

        String createdId = indexResponse.getId();
        User createdResource = findById(createdId);

        return createdResource;
    }

    public Integer delete(String id) {
        DeleteRequest deleteRequest = new DeleteRequest("users", id);
        deleteRequest.timeout("10s");

        DeleteResponse deleteResponse = null;
        try
        {
            deleteResponse = ESClient.delete(deleteRequest, RequestOptions.DEFAULT);
        }
        catch (IOException ex)
        {
            logger.error("Cannot delete resource: " + ex);
            return 2;
        }

        ReplicationResponse.ShardInfo shardInfo = deleteResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return 1;
        }

        if (deleteResponse.getResult() != DocWriteResponse.Result.DELETED) {
            logger.error("Failed to delete resource: " + deleteResponse.getResult());
            return 1;
        }

        return 0;
    }

    public User update(String id, User user) {
        Map<String, Object> userMap = getUserAsJson(user);

        IndexRequest indexRequest = new IndexRequest("users").id(id).source(userMap);
        indexRequest.timeout("10s");
        indexRequest.opType(DocWriteRequest.OpType.INDEX);

        IndexResponse indexResponse = null;
        try
        {
            indexResponse = ESClient.index(indexRequest, RequestOptions.DEFAULT);
        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.CONFLICT)
                logger.error("Creating resource is not allowed.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot modify resource: " + ex);
            return null;
        }

        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();

        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                String reason = failure.reason();
                logger.error("Shard failed with reason: " + reason);
            }
            return null;
        }

        if (indexResponse.getResult() != DocWriteResponse.Result.UPDATED)
        {
            logger.error("Failed to modify resource: " + indexResponse.getResult());
            return null;
        }

        String modifiedId = indexResponse.getId();
        User modifiedResource = findById(modifiedId);

        return modifiedResource;
    }

    public User findById(String id) {
        GetRequest getRequest = new GetRequest("users", id);
        GetResponse getResponse = null;

        try
        {
            getResponse = ESClient.get(getRequest, RequestOptions.DEFAULT);
        }
        catch (ElasticsearchException ex)
        {
            if (ex.status() == RestStatus.NOT_FOUND)
                logger.error("Cannot find index.");
            return null;
        }
        catch (IOException ex)
        {
            logger.error("Cannot get resource: " + ex);
            return null;
        }

        if (!getResponse.isExists()) {
            logger.error("The specified resource does not exists.");
            return null;
        }

        String resourceId = getResponse.getId();
        String sourceAsString = getResponse.getSourceAsString();
        User user = getJsonAsUser(sourceAsString, resourceId);

        return user;
    }

    private Map<String, Object> getUserAsJson(User user) {
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("name", user.getName());
        userMap.put("enterprise", user.getEnterprise());
        userMap.put("position", user.getLatitude() + "," + user.getLongitude());
        return userMap;
    }

    private User getJsonAsUser(String userAsString, String id) {
        JSONObject userAsJson = new JSONObject(userAsString);
        User user = new User();

        user.setId(Long.parseLong(id));
        user.setName(userAsJson.getString("name"));
        user.setEnterprise(userAsJson.getString("enterprise"));

        String position = userAsJson.getString("position");
        String allPositions[] = position.split(",");
        Double latitude = Double.parseDouble(allPositions[0]);
        Double longitude = Double.parseDouble(allPositions[1]);
        user.setLatitude(latitude);
        user.setLongitude(longitude);

        return user;
    }
}
