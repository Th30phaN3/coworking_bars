package com.coworking.coworking.user;

public interface UserService {

    User create(User user);

    Integer delete(String id);

    User update(String id, User user);

    User findById(String id);
}
