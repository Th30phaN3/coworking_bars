package com.coworking.coworking.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public User create(User user) {
        return userRepository.create(user);
    }

    public Integer delete(String id) {
        return userRepository.delete(id);
    }

    public User update(String id, User user) {
        return userRepository.update(id, user);
    }

    public User findById(String id) {
        return userRepository.findById(id);
    }
}
