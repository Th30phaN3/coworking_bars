package com.coworking.coworking.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Api(description = "CRUD operations on users", consumes = "application/json", produces = "application/json")
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired UserService userService;

    @ApiOperation(value = "Insert a new user")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Resource created"),
        @ApiResponse(code = 500, message = "Cannot create the resource") })
    @PostMapping
    public ResponseEntity<?> create(@RequestBody User user) {
        User result = userService.create(user);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(result.getId())
            .toUri();

        // Send location in response
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Delete a user")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource deleted"),
        @ApiResponse(code = 404, message = "Cannot find resource"),
        @ApiResponse(code = 500, message = "Cannot delete the resource") })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id) {
        Integer result = userService.delete(id);

        if (result == 1)
            return ResponseEntity.notFound().build();

        if (result == 2)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Modify a user")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Resource modified"),
        @ApiResponse(code = 500, message = "Cannot modify the resource") })
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody User user) {
        User result = userService.update(id, user);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Find a user with its identifier")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource found"),
        @ApiResponse(code = 404, message = "Cannot find resource") })
    @GetMapping("/{id}")
    public ResponseEntity<?> findUserById(@PathVariable("id") String id) {
        User result = userService.findById(id);

        if (result == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
