package com.coworking.coworking.user;

import io.swagger.annotations.ApiModelProperty;

public class User {

    @ApiModelProperty(notes = "Identifier", allowableValues = "range[0,infinity]", example = "42")
    private long id;

    @ApiModelProperty(notes = "User name", required = true, example = "John Smith")
    private String name;

    @ApiModelProperty(notes = "Latitude (WGS84)", required = true, example = "47.216")
    private Double latitude;

    @ApiModelProperty(notes = "Longitude (WGS84)", required = true, example = "-1.549")
    private Double longitude;

    @ApiModelProperty(notes = "User enterprise", required = true, example = "ThisIsATest")
    private String enterprise;

    public User()
    {
        super();
    }

    public Long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() { return latitude; }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }
}
